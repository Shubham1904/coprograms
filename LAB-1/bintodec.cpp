//Shubham Narvekar
#include <iostream>
using namespace std;
void DecToBin(int n)
{
    int x=n;
    int b[1000];
    int i=0;
    while(n>0)
    {
        b[i]=n%2;
        n=n/2;
        i++;
    }
    cout<<"\nThe Binary Equivalent of "<<x<<" is ";
    for(int j=i-1;j>=0;j--)
    cout<<b[j];
    cout<<"\n\n";
    }
int BinToDec(int n)
{
    int num=n;
    int d=0;
    int base=1;
    int temp=num;
    while(temp)
    {
        int last_digit=temp%10;
        temp=temp/10;d+=last_digit*base;
        base=base*2;
    }
    return d;
}
int main()
{
    int n,ch;
    while(1)
    {
        cout<<"\n1: To convert Decimal to Binary";
        cout<<"\n2: To convert Binary to Decimal";
        cout<<"\n3: To Exit";
        cout<<"\n\nEnter your choice \t";
        cin>>ch;
        switch(ch)
        {
            case 1:cout<<"\nEnter a Decimal Number \t";
            cin>>n;
            DecToBin(n);
            break;
            case 2:cout<<"\nEnter a Binary Number \t";
            cin>>n;
            cout<<"\nThe Binary Equivalent of "<<n<<" is "<<BinToDec(n);
            break;
            case 3:return 0;
        }
    }
    return 0;
}
